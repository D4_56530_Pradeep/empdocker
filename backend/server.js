const express = require('express')

const app = express()

app.use(express.json())

const route = require('./routes/emp')
app.use('/emp', route)

app.listen(4000, '0.0.0.0', () => {
    console.log('backend server started on port 4000')
})