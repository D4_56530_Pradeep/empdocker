const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router =  express.Router()


router.get('/sys', (request, response) => {
    response.send('Checking....')
})

router.get('/', (request, response) => {
    const statement = `SELECT * FROM emp `

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/', (request, response) => {
    const {name, age, salary} = request.body

    const statement = `INSERT INTO emp(name, age, salary) VALUES('${name}', ${age}, ${salary}) `

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.put('/:id', (request, response) => {
    const {salary} = request.body
    const {id} = request.params

    const statement = `UPDATE emp SET salary = ${salary} WHERE id = ${id} `

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (request, response) => {
    const {id} = request.params

    const statement = `DELETE from emp WHERE id = ${id} `

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router