CREATE TABLE emp (
  id INTEGER PRIMARY KEY auto_increment,
  name VARCHAR(20),
  age int,
  salary int
);

INSERT INTO emp (name, age, salary) VALUES ('Pradeep', 25, 30000);
INSERT INTO emp (name, age, salary) VALUES ('Swapnil', 25, 500000);